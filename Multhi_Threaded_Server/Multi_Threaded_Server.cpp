#include "Multi_Threaded_Server.h"

//c\d'tor:
Multi_Threaded_Server::Multi_Threaded_Server()
{
	WSADATA info;
	if (WSAStartup(MAKEWORD(2, 0), &info))
		cout << "WSAStartup" << endl;
	s1 = new server();
}
Multi_Threaded_Server::~Multi_Threaded_Server()
{
	delete s1;
	for (clients_it = clients.begin(); clients_it != clients.end(); clients_it++)
	{
		if (clients_it->joinable())
			clients_it->join();
	}
	if (WSACleanup() == SOCKET_ERROR)
		cout << "WSAclenup failed" << endl;
}

//funcs:
void Multi_Threaded_Server::listener()
{
	for (int i = 0; i < 4; i++)
	{
		s1->Slisten();
		s1->Saccept(&clientsocket, &myclient);
		clients.push_back(thread(server::HandleClient, clientsocket));
	}
}
void Multi_Threaded_Server::Handle_Client_Caller()
{
	server::HandleClient(clientsocket);
}
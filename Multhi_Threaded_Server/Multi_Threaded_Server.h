#pragma once

# include "server.h"
# include<vector>
# include<algorithm>
# include<thread>

class Multi_Threaded_Server
{
public:
	Multi_Threaded_Server();
	~Multi_Threaded_Server();
	void listener();

private:
	vector<thread> clients;
	vector<thread>::iterator clients_it;

	server *s1;
	sock clientsocket;
	sockaddi myclient;

	void Handle_Client_Caller();
};


#include "server.h"

//c\d'tor:
server::server()
{
	mysocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (mysocket == INVALID_SOCKET)
		func_failed("socket");

	myserver.sin_family = AF_INET;
	myserver.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");
	myserver.sin_port = htons(65333);

	if (bind(mysocket, (sockaddr*)&myserver, sizeof(myserver)) == SOCKET_ERROR)
		sock_func_failed("bind", mysocket);
	cout << "bind... Done" << endl;

	sai_size = sizeof(sockaddi);
}
server::~server()
{
	if (closesocket(mysocket) == SOCKET_ERROR)
		func_failed("second closesocket");
}

//funcs
void server::Slisten()
{
	if (listen(mysocket, SOMAXCONN) == SOCKET_ERROR)
		sock_func_failed("listen", mysocket);
	cout << "listening..." << endl;
}
void server::HandleClient(sock csocket)
{
	Ssend("accept", csocket);
	closeClient(csocket);
}
void server::Saccept(sock *clientsocket, sockaddi *client)
{
	*clientsocket = accept(mysocket, (sockaddr*)client, &sai_size);
	if (*clientsocket == INVALID_SOCKET)
		sock_func_failed("accept/n/0", mysocket);
	cout << "connection accepted!" << endl;

}
void server::Ssend(string msg, sock csocket)
{
	if (send(csocket, msg.c_str(), msg.length(), 0) == SOCKET_ERROR)
	{
		cout << WSAGetLastError() << endl;
		func_failed("send");
	}
	cout << "the message has been sent." << endl;
}
void server::closeClient(sock csocket)
{
	if (closesocket(csocket) == SOCKET_ERROR)
		func_failed("first closesocket");
}

//in case of failure...
void server::func_failed(string func)
{
	cout << endl << func << " function failed.\n";
	system("PAUSE");
	exit(1);
}
void server::sock_func_failed(string func, sock mysocket)
{
	cout << endl << func << " function failed.\n";
	int check = closesocket(mysocket);
	if (check == SOCKET_ERROR)
	{
		cout << "\nclosesocket function failed.\n";
		system("PAUSE");
		exit(1);
	}
	check = WSACleanup();
	if (check == SOCKET_ERROR)
	{
		cout << "\nWSAclenup function failed.\n";
		system("PAUSE");
		exit(1);
	}
	system("PAUSE");
	exit(1);
}


#pragma once

# include<iostream>
# include<string>
# include<WinSock2.h>
# include<Windows.h>

#pragma comment(lib,"ws2_32.lib")

using namespace std;

typedef int sock;
typedef struct sockaddr_in sockaddi;

class server
{
public:
	server();
	~server();
	void Slisten();
	static void HandleClient(sock clientsocket);
	void Saccept(sock *clientsocket, sockaddi *client);

	static void Ssend(string msg, sock csocket);
	//static void Srecv();
	static void closeClient(sock csocket);

	static void func_failed(string func);
	static void sock_func_failed(string func, sock mysocket);
private:
	sock mysocket;
	sockaddi myserver;
	int sai_size;
};


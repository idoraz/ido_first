#include <iostream>
#include <fstream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <string>
#include <vector>


using namespace std;

void ReaderThread();
void WriterThread(string str);
bool isEmpty();//tells if the file is empty(true) or not (false)

int readersCounter = 0;
condition_variable cv;
mutex mtx;
fstream fs;

void main()
{
	fs.open("text.txt", fstream::in | fstream::out | fstream::app);
	thread r1(ReaderThread);
	thread r2(ReaderThread);
	thread w1(WriterThread, "1111111\n112");
	thread r3(ReaderThread);
	thread w2(WriterThread, "2222222");
	thread w3(WriterThread, "3333333");

	r1.join();
	r2.join();
	r3.join();
	w1.join();
	w2.join();
	w3.join();
	fs.close();
	system("pause");
}

void ReaderThread()
{
	unique_lock<mutex> lock1(mtx);
	cout << "reader waiting..." << endl;
	readersCounter++;
	cv.wait(lock1, []{return (!isEmpty()); });//if the file isn't empty the readers have privilegy
	cout << "reader starting!" << endl;

	fs.seekg(0, fs.end);
	int flen = fs.tellg();
	fs.seekg(0, fs.beg);
	char *data = new char[flen + 1];//buffer to save the data. the buffers len is in the size of the data in the file.
	for (int i = 0; i < flen + 1; i++)
		data[i] = NULL;//zero the memory
	fs.read(data, flen);// reading the file content and save it in str
	cout << data << endl;//prints to screen the data that was readen

	fs.close();
	fs.open("text.txt", fstream::in | fstream::out | fstream::trunc); //delete the data from the file
	
	cout << "reader ended!" << endl;
	readersCounter--;
	lock1.unlock();
	cv.notify_one();
}

void WriterThread(string str)
{
	unique_lock<mutex> lock1(mtx);
	cout << "writer waiting..." << endl;
	cv.wait(lock1, []{return ( isEmpty() || (readersCounter == 0)); });// the writer will start if no readers exist or the file is empty
	
	cout << "writer starting!" << endl;
	fs << str;
	cout << "writer ended!" << endl;

	lock1.unlock();
	cv.notify_one();
}

bool isEmpty()
{
	fs.seekg(0, fs.end);
	return (int)fs.tellg() == 0;
}
# define _CRT_SECURE_NO_WARNINGS
# include<iostream>
# include<string>
# include<WinSock2.h>
# include<Windows.h>

#pragma comment(lib,"ws2_32.lib")

using namespace std;

typedef int sock;
typedef struct sockaddr_in sockaddi;

void sock_func_failed(string func, sock mysocket);
void func_failed(string func);

int main()
{
	WSADATA info;
	int check;
	sock mysocket, clientsocket;
	sockaddi myserver, myclient;
	string buf = "Accepted\n\0";


	if (WSAStartup(MAKEWORD(2, 0), &info))
	{
		func_failed("WSAStartup");
	}



	mysocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (mysocket == INVALID_SOCKET)
	{
		func_failed("socket");
	}
	myserver.sin_family = AF_INET;
	myserver.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");
	myserver.sin_port = htons(65333);



	if (bind(mysocket, (sockaddr*)&myserver, sizeof(myserver)) == SOCKET_ERROR)
	{
		sock_func_failed("bind", mysocket);
	}
	cout << "bind... Done" << endl;
	if (listen(mysocket, SOMAXCONN) == SOCKET_ERROR)
	{
		sock_func_failed("listen", mysocket);
	}
	cout << "listening..." << endl;
	int x = sizeof(sockaddi);
	clientsocket = accept(mysocket, (sockaddr*)&myclient, &x);
	if (clientsocket == INVALID_SOCKET)
	{
		sock_func_failed("accept", mysocket);
	}
	cout << "connection accepted!" << endl;
	


	if (send(clientsocket, buf.c_str(), buf.length(), 0) == SOCKET_ERROR)
	{
		func_failed("send");
	}
	cout << "the message has been sent." << endl;


	if (closesocket(clientsocket) == SOCKET_ERROR)
	{
		func_failed("first closesocket");
	}
	if (closesocket(mysocket) == SOCKET_ERROR)
	{
		func_failed("second closesocket");
	}
	if (WSACleanup() == SOCKET_ERROR)
	{
		func_failed("WSAclenup");
	}
	system("PAUSE");
	return 0;
}

void sock_func_failed(string func, sock mysocket)
{
	cout << endl<<func<<" function failed.\n";
	int check = closesocket(mysocket);
	if (check == SOCKET_ERROR)
	{
		cout << "\nclosesocket function failed.\n";
		system("PAUSE");
		exit(1);
	}
	check = WSACleanup();
	if (check == SOCKET_ERROR)
	{
		cout << "\nWSAclenup function failed.\n";
		system("PAUSE");
		exit(1);
	}
	system("PAUSE");
	exit(1);
}

void func_failed(string func)
{
	cout << endl<< func <<" function failed.\n";
	system("PAUSE");
	exit(1);
}
